package com.example.demo.security.jwt;


import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.stereotype.Service;

import com.example.demo.config.AppProperties;
import com.example.demo.dto.LocalUser;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
 
@Service
public class TokenProvider {
	
	
	 private static final Logger logger = LoggerFactory.getLogger(TokenProvider.class);
	 
	    private AppProperties appProperties;
	 
	    public TokenProvider(AppProperties appProperties) {
	        this.appProperties = appProperties;
	    }
	 
	    public String createToken(Authentication authentication) {
	        LocalUser userPrincipal = (LocalUser) authentication.getPrincipal();
	        Date now = new Date();
	        System.out.println("date  now" + now);
	        Date expiryDate = new Date(now.getTime() + appProperties.getAuth().getTokenExpirationMsec());
	        System.out.println("date  expire" + expiryDate);
	        return Jwts.builder().setSubject(Long.toString(userPrincipal.getUser().getId())).setIssuedAt(new Date()).setExpiration(expiryDate)
	                .signWith(SignatureAlgorithm.HS256, appProperties.getAuth().getTokenSecret()).compact();
	    }
	 
	    public Long getUserIdFromToken(String token) {
	        Claims claims = Jwts.parser().setSigningKey(appProperties.getAuth().getTokenSecret()).parseClaimsJws(token).getBody();
	 
	        return Long.parseLong(claims.getSubject());
	    }
	  
	 
	    public boolean validateToken(String authToken) {
	        try {
	            Jwts.parser().setSigningKey(appProperties.getAuth().getTokenSecret()).parseClaimsJws(authToken);
	            return true;
	        } catch (SignatureException ex) {
	            logger.error("Invalid JWT signature");
	        } catch (MalformedJwtException ex) {
	            logger.error("Invalid JWT token");
	        } catch (ExpiredJwtException ex) {
	            logger.error("Expired JWT token");
	        } catch (UnsupportedJwtException ex) {
	            logger.error("Unsupported JWT token");
	        } catch (IllegalArgumentException ex) {
	            logger.error("JWT claims string is empty.");
	        }
	        return false;
	    }

}
