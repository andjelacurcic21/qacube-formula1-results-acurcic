package com.example.demo.formula;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Driver {
	
	private String driverId;
	private String GivenName;
	private String FamilyName;
	private String DateOfBirth;
	private String Nationality;
	
	public Driver() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Driver(String driverId, String givenName, String familyName, String dateOfBirth, String nationality) {
		super();
		this.driverId = driverId;
		this.GivenName = givenName;
		this.FamilyName = familyName;
		this.DateOfBirth = dateOfBirth;
		this.Nationality = nationality;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public String getGivenName() {
		return GivenName;
	}

	public void setGivenName(String givenName) {
		this.GivenName = givenName;
	}

	public String getFamilyName() {
		return FamilyName;
	}

	public void setFamilyName(String familyName) {
		this.FamilyName = familyName;
	}

	public String getDateOfBirth() {
		return DateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.DateOfBirth = dateOfBirth;
	}

	public String getNationality() {
		return Nationality;
	}

	public void setNationality(String nationality) {
		this.Nationality = nationality;
	}

	@Override
	public String toString() {
		return "Driver [driverId=" + driverId + ", GivenName=" + GivenName + ", FamilyName=" + FamilyName
				+ ", DateOfBirth=" + DateOfBirth + ", Nationality=" + Nationality + "]";
	}

	
	
	
	
}
