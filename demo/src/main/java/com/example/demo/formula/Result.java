package com.example.demo.formula;



import java.sql.Time;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Result {

	private int number;
	private int position;
	private int points;
	private Driver driver;
	private Constructor constructor;
	
	private String time;
	
	
	//stream java 8

	public Result() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Result(int number, int position, int points, Driver driver, Constructor constructor, String time) {
		super();
		this.number = number;
		this.position = position;
		this.points = points;
		this.driver = driver;
		this.constructor = constructor;
		this.time = time;
	}



	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Constructor getConstructor() {
		return constructor;
	}

	public void setConstructor(Constructor constructor) {
		this.constructor = constructor;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}
	
	
	
	
	
}
