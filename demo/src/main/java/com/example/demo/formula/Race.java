package com.example.demo.formula;

import java.util.List;

public class Race {
	
	private int season;
	private int round;
	private String name;
	private List<Result> results;
	
	public Race() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Race(int season, int round, List<Result> results) {
		super();
		this.season = season;
		this.round = round;
		this.results = results;
	}

	

	public Race(int season, int round, String name, List<Result> results) {
		super();
		this.season = season;
		this.round = round;
		this.name = name;
		this.results = results;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getSeason() {
		return season;
	}


	public void setSeason(int season) {
		this.season = season;
	}


	public int getRound() {
		return round;
	}


	public void setRound(int round) {
		this.round = round;
	}


	public List<Result> getResults() {
		return results;
	}
	public void setResults(List<Result> results) {
		this.results = results;
	}
	
	
	

}
