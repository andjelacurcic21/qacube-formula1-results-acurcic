package com.example.demo.formula;

public class Constructor {

	private String ConstructorId;
	private String Name;
	private String Nationality;
	
	public Constructor() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Constructor(String constructorId, String name, String nationality) {
		super();
		this.ConstructorId = constructorId;
		this.Name = name;
		this.Nationality = nationality;
	}
	public String getConstructorId() {
		return ConstructorId;
	}
	public void setConstructorId(String constructorId) {
		this.ConstructorId = constructorId;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		this.Name = name;
	}
	public String getNationality() {
		return Nationality;
	}
	public void setNationality(String nationality) {
		this.Nationality = nationality;
	}
	
	
	
}
