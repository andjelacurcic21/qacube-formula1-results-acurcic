package com.example.demo.service;

import com.example.demo.dto.LocalUser;
import com.example.demo.dto.SignUpRequest;
import com.example.demo.exception.UserAlreadyExistAuthenticationException;
import com.example.demo.model.User;
import java.util.Map;
import java.util.Optional;
 
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
 


public interface UserService {
	
	public User registerNewUser(SignUpRequest signUpRequest) throws UserAlreadyExistAuthenticationException;
	 
    User findUserByEmail(String email);
 
    Optional<User> findUserById(Long id);
 
    LocalUser processUserRegistration(String registrationId, Map<String, Object> attributes, OidcIdToken idToken, OidcUserInfo userInfo);
}



