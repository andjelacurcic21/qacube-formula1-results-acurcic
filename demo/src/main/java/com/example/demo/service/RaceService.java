package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo.formula.Race;
import com.example.demo.formula.Result;

@Service
public class RaceService {
	
	@Autowired
	DriverService driverService;
	@Autowired
	ConstructorService constructorService;
	

	
	public List<Race> allRaces(){
		
		final String uri = "https://ergast.com/api/f1/" + "results";
		
		RestTemplate restTemplate = new RestTemplate();
		
		String racej = restTemplate.getForObject(uri,String.class);
		JSONObject json = XML.toJSONObject(racej);
		
		JSONArray jsonArray=  json.getJSONObject("MRData").getJSONObject("RaceTable").getJSONArray("Race");
		List<Race> races = new ArrayList<Race>();
		
		for (int i=0; i<jsonArray.length(); i++) {
			
			Race race = new Race();
			JSONObject j = jsonArray.getJSONObject(i);
			race.setSeason(j.getInt("season"));
			race.setRound(j.getInt("round"));
			JSONArray help = j.getJSONObject("ResultsList").getJSONArray("Result");
			ArrayList<Result> results = new ArrayList<Result>();
			for(int k=0; k<help.length(); k++) {
				Result result = new Result();
				JSONObject jo = help.getJSONObject(k);
				result.setNumber(jo.getInt("number"));
				result.setPosition(jo.getInt("position"));
				

				result.setPoints(jo.getInt("points"));
				result.setDriver(driverService.findOne((jo.getJSONObject("Driver").getString("driverId"))));
				result.setConstructor(constructorService.findById(jo.getJSONObject("Constructor").getString("constructorId")));
				
				if(jo.optJSONObject("Time") != null) {
					result.setTime(jo.optJSONObject("Time").getString("content"));

				}else {
					result.setTime("Nema vreme");
				}
				results.add(result);
			}
			race.setResults(results);
			races.add(race);	
		}		
		System.out.println(races.size());

		return races;
	}

	public List<Race> allResultsBySeason(int season) {
		
		final String uri = "https://ergast.com/api/f1/" + season + "/results?limit=100";
		
		RestTemplate restTemplate = new RestTemplate();
		
		String racej = restTemplate.getForObject(uri,String.class);
		JSONObject json = XML.toJSONObject(racej);
		
		JSONArray jsonArray=  json.getJSONObject("MRData").getJSONObject("RaceTable").getJSONArray("Race");
		List<Race> races = new ArrayList<Race>();
		
		for (int i=0; i<jsonArray.length(); i++) {
			
			Race race = new Race();
			JSONObject j = jsonArray.getJSONObject(i);
			race.setSeason(j.getInt("season"));
			race.setRound(j.getInt("round"));
			JSONArray help = j.getJSONObject("ResultsList").getJSONArray("Result");
			ArrayList<Result> results = new ArrayList<Result>();
			for(int k=0; k<help.length(); k++) {
				Result result = new Result();
				JSONObject jo = help.getJSONObject(k);
				result.setNumber(jo.getInt("number"));
				result.setPosition(jo.getInt("position"));
				

				result.setPoints(jo.getInt("points"));
				result.setDriver(driverService.findOne((jo.getJSONObject("Driver").getString("driverId"))));
				result.setConstructor(constructorService.findById(jo.getJSONObject("Constructor").getString("constructorId")));
				
				if(jo.optJSONObject("Time") != null) {
					result.setTime(jo.optJSONObject("Time").getString("content"));

				}else {
					result.setTime("Nema vreme");
				}
				results.add(result);
			}
			race.setResults(results);
			races.add(race);	
		}		
		System.out.println(races.size());

		return races;
	}

	
	public Race allResultsBySeasonAndRound(int season, int round) {
		final String uri = "https://ergast.com/api/f1/"+ season +"/"+ round + "/results?limit=1000";
		
		RestTemplate restTemplate = new RestTemplate();
		
		String racej = restTemplate.getForObject(uri,String.class);
		JSONObject json = XML.toJSONObject(racej);
		
		JSONObject raceob=  json.getJSONObject("MRData").getJSONObject("RaceTable").getJSONObject("Race");
		//List<Race> races = new ArrayList<Race>();
		
			Race race = new Race();
			race.setSeason(raceob.getInt("season"));
			race.setRound(raceob.getInt("round"));
			race.setName(raceob.getString("RaceName"));
			JSONArray help = raceob.getJSONObject("ResultsList").getJSONArray("Result");
			ArrayList<Result> results = new ArrayList<Result>();
			for(int k=0; k<help.length(); k++) {
				Result result = new Result();
				JSONObject jo = help.getJSONObject(k);
				result.setNumber(jo.getInt("number"));
				result.setPosition(jo.getInt("position"));
				

				result.setPoints(jo.getInt("points"));
				result.setDriver(driverService.findOne((jo.getJSONObject("Driver").getString("driverId"))));
				result.setConstructor(constructorService.findById(jo.getJSONObject("Constructor").getString("constructorId")));
				
				if(jo.optJSONObject("Time") != null) {
					result.setTime(jo.optJSONObject("Time").getString("content"));

				}else {
					result.setTime("Nema vreme");
				}
				results.add(result);
			}
			race.setResults(results);
			
		System.out.println(race.getName());
		

		
		return race;
	}

	public List<Race> resultsByDriver(int season, String id) {
		
		final String uri = "https://ergast.com/api/f1/" + season +"/drivers/"+ id + "/results";
		RestTemplate restTemplate = new RestTemplate();
		
		String racej = restTemplate.getForObject(uri,String.class);
		JSONObject json = XML.toJSONObject(racej);
		JSONObject rac;
		System.out.println( rac = json.getJSONObject("MRData").getJSONObject("RaceTable").getJSONObject("Races"));
		
		JSONArray raceArray=  json.getJSONObject("MRData").getJSONObject("RaceTable").getJSONArray("Races");
		ArrayList<Race> races = new ArrayList<Race>();
		for(int i =0; i<raceArray.length(); i++) {
			Race race = new Race();
			JSONObject jo = raceArray.getJSONObject(i);
			race.setRound(jo.getInt("round"));
			race.setName(jo.getString("raceName"));
			List<Result> res = new ArrayList<Result>();
			JSONArray help = jo.getJSONArray("Results");
			for( int k = 0; k< help.length(); k++) {
				JSONObject m = help.getJSONObject(k);
				Result result = new Result();
				result.setPosition(m.getInt("position"));
				result.setPoints(m.getInt("points"));
				result.setNumber(m.getInt("number"));
				result.setDriver(driverService.findOne((jo.getJSONObject("Driver").getString("driverId"))));
				if(jo.optJSONObject("Time") != null) {
					result.setTime(jo.optJSONObject("Time").getString("content"));

				}else {
					result.setTime("Nema vreme");
				}
				res.add(result);
			}
			races.add(race);
		}
		return races;
	}

}
