package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo.formula.Constructor;
import com.example.demo.formula.Driver;

@Service
public class ConstructorService {
	
	public List<Constructor> allConstructors(){
		
	final String uri = "https://ergast.com/api/f1/constructors?limit=500";
	
	RestTemplate restTemplate = new RestTemplate();
	
	String constructors = restTemplate.getForObject(uri,String.class);
	JSONObject json = XML.toJSONObject(constructors);
	
	JSONArray constructors2 = json.getJSONObject("MRData").getJSONObject("ConstructorTable").getJSONArray("Constructor");
	List<Constructor> constructorList = new ArrayList<Constructor>();
	
	
		for (int i=0; i<constructors2.length(); i++) {
		
		Constructor constructor = new Constructor();
		JSONObject d = constructors2.getJSONObject(i);
		constructor.setConstructorId(d.getString("constructorId"));
		constructor.setName(d.getString("Name"));
		constructor.setNationality(d.getString("Nationality"));
		constructorList.add(constructor);
	}
	return constructorList;
}
	

	
	public List<Constructor> findAll(){
		
	List<Constructor> constructors = allConstructors();
	
	return constructors;
		
	}
	
	public Constructor findById(String id) {
		List<Constructor> constructors = allConstructors();
		Constructor constructor = new Constructor();

		for(int i =0; i<constructors.size(); i++) {
			if(constructors.get(i).getConstructorId().equals(id)) {
				constructor.setConstructorId(constructors.get(i).getConstructorId());
				constructor.setName(constructors.get(i).getName());
				constructor.setNationality(constructors.get(i).getNationality());
			}
		}
		return constructor;
		
	}

}
