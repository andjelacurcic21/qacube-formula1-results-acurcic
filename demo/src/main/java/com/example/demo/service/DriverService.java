package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo.formula.Driver;

import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

@Service
public class DriverService {
	
	
	
	public List<Driver> allDrivers(){
		final String uri = "https://ergast.com/api/f1/drivers?limit=1000";
		RestTemplate restTemplate = new RestTemplate();
		
		String drivers = restTemplate.getForObject(uri,String.class);
		JSONObject json = XML.toJSONObject(drivers);
		
		JSONArray drivers2 = json.getJSONObject("MRData").getJSONObject("DriverTable").getJSONArray("Driver");
		List<Driver> driversList = new ArrayList<Driver>();
		
		
		for (int i=0; i<drivers2.length(); i++) {
			
			Driver driver = new Driver();
			JSONObject d = drivers2.getJSONObject(i);
			driver.setDriverId(d.getString("driverId"));
			driver.setFamilyName(d.getString("FamilyName"));
			driver.setDateOfBirth(d.getString("DateOfBirth"));
			driver.setGivenName(d.getString("GivenName"));
			driver.setNationality(d.getString("Nationality"));
			driversList.add(driver);
		}
		
		return driversList;
	}
	
	public List<Driver> findAll()  {
		
		
		List<Driver> drivers = allDrivers(); 		
		
		return drivers;
	
	}

	public Driver findOne(String id) {
		
		List<Driver> drivers = allDrivers(); 
		Driver driver = new Driver();
		
		//System.out.println(id);
		
		for (int i=0; i<drivers.size(); i++) {
			if(drivers.get(i).getDriverId().equals(id)) {
				driver.setDriverId( drivers.get(i).getDriverId());
				driver.setFamilyName(drivers.get(i).getFamilyName());
				driver.setDateOfBirth(drivers.get(i).getDateOfBirth());
				driver.setGivenName(drivers.get(i).getGivenName());
				driver.setNationality(drivers.get(i).getNationality());
			}
			
		}
		return driver;
		
		
	}

	public List<Driver> findAllBySeason(int season) {
		
		final String uri = "https://ergast.com/api/f1/" + season + "/drivers?limit=1000";
	
		RestTemplate restTemplate = new RestTemplate();
		
		String drivers = restTemplate.getForObject(uri,String.class);
		JSONObject json = XML.toJSONObject(drivers);
		
		JSONArray drivers2 = json.getJSONObject("MRData").getJSONObject("DriverTable").getJSONArray("Driver");
		List<Driver> driversList = new ArrayList<Driver>();
		
		
		for (int i=0; i<drivers2.length(); i++) {
			
			Driver driver = new Driver();
			JSONObject d = drivers2.getJSONObject(i);
			driver.setDriverId(d.getString("driverId"));
			driver.setFamilyName(d.getString("FamilyName"));
			driver.setDateOfBirth(d.getString("DateOfBirth"));
			driver.setGivenName(d.getString("GivenName"));
			driver.setNationality(d.getString("Nationality"));
			driversList.add(driver);
		}
		
		return driversList;
		
	}
	
	

}
