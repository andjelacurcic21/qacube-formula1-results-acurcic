package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.demo.formula.Driver;
import com.example.demo.formula.Result;

@Service
public class ResultService {
	
	@Autowired
	DriverService driverService;
	
	@Autowired
	ConstructorService constructorService;
	
	public List<Result> allResults(int season, int round){
		
		final String uri = "https://ergast.com/api/f1/" + season + "/" + round + "/results";

		
		RestTemplate restTemplate = new RestTemplate();
		
		String results = restTemplate.getForObject(uri,String.class);
		JSONObject json = XML.toJSONObject(results);
		
		
		
		JSONArray jsonArray=  json.getJSONObject("MRData")
				.getJSONObject("RaceTable")
				.getJSONObject("Race")
				.getJSONObject("ResultsList")
				.getJSONArray("Result");
		
		System.out.println(jsonArray);
		List<Result> resultsList = new ArrayList<Result>();
		
		for (int i=0; i<jsonArray.length(); i++) {
			
			Result result = new Result();
			JSONObject j = jsonArray.getJSONObject(i);
			result.setNumber(j.getInt("number"));
			result.setPosition(j.getInt("position"));
			result.setPoints(j.getInt("points"));
			result.setDriver(driverService.findOne((j.getJSONObject("Driver").getString("driverId"))));
			result.setConstructor(constructorService.findById(j.getJSONObject("Constructor").getString("constructorId")));
			
			//System.out.println("neesto" + j.getJSONObject("Time").getString("content"));
			//result.setTime(j.getJSONObject("Time").getNumber("millis").toString());
			resultsList.add(result);
			System.out.println(result);
		}
		
		return resultsList;
	}
	
	
	public List<Result>findAll(){
		
		return null;
	}
	
	public Result findOne() {
		
		return null;
	}



}
