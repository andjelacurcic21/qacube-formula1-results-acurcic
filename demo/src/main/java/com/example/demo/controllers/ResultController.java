package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.formula.Result;
import com.example.demo.service.ResultService;

@RestController
@RequestMapping(value = "/f1", produces = MediaType.APPLICATION_JSON_VALUE)
public class ResultController {
	
	@Autowired
	ResultService resultService;
	
	@GetMapping(value = "/{season}/{round}/results")
	public ResponseEntity<List<Result>> drivers(@PathVariable int season,@PathVariable int round) {
		
		List<Result> results = resultService.allResults(season, round);
		
		return new ResponseEntity<>(results,HttpStatus.OK);
		
	}
	
	
	
	

}
