package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.formula.Constructor;
import com.example.demo.formula.Driver;
import com.example.demo.service.ConstructorService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
@RequestMapping(value = "api/f1/constructors", produces = MediaType.APPLICATION_JSON_VALUE)
public class ConstructorController {
	
	@Autowired
	ConstructorService constructorService;
	
	@GetMapping
	public ResponseEntity<List<Constructor>> constructors() throws JsonMappingException, JsonProcessingException{
		
		List<Constructor> constructors = constructorService.findAll();
		
		return new ResponseEntity<>(constructors,HttpStatus.OK);
		
	}
	
	@GetMapping("/{id}")
    public ResponseEntity<Constructor> getOne(@PathVariable String id){
		Constructor constructor = constructorService.findById(id);
       
        if(constructor != null) {
            return new ResponseEntity<>(constructor, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
