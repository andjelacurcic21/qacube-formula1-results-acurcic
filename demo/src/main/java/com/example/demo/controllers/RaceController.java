package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.formula.Race;
import com.example.demo.formula.Result;
import com.example.demo.service.RaceService;
import com.example.demo.service.ResultService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/f1/race", produces = MediaType.APPLICATION_JSON_VALUE)
public class RaceController {
	
	@Autowired
	RaceService raceService;
	
	@GetMapping
	public ResponseEntity<List<Race>> results() {
		
		List<Race> results = raceService.allRaces();		
		return new ResponseEntity<>(results,HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/{season}")
	public ResponseEntity<List<Race>> results(@PathVariable int season) {
		
		List<Race> results = raceService.allResultsBySeason(season);
		
		return new ResponseEntity<>(results,HttpStatus.OK);
		
	}
	
	
	@GetMapping(value = "/{season}/{round}")
	public ResponseEntity<Race> results2(@PathVariable int season,@PathVariable int round) {
		
		Race results = raceService.allResultsBySeasonAndRound(season, round);
		
		return new ResponseEntity<>(results,HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/{season}/drivers/{id}")
	public ResponseEntity<List<Race>> resultsDriver(@PathVariable int season, @PathVariable String id) {
		
		List<Race> results = raceService.resultsByDriver(season,id);
		
		return new ResponseEntity<>(results,HttpStatus.OK);
		
	}
	
	

}
