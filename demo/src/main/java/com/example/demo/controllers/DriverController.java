package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.formula.Driver;
import com.example.demo.service.DriverService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;




@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/f1/drivers", produces = MediaType.APPLICATION_JSON_VALUE)
public class DriverController {
	
	@Autowired
	DriverService driverService;
	
	@PreAuthorize("hasRole('USER')")
	@GetMapping
	public ResponseEntity<List<Driver>> drivers() throws JsonMappingException, JsonProcessingException{
		
		List<Driver> driver = driverService.findAll();
		
		return new ResponseEntity<>(driver,HttpStatus.OK);
		
	}
	
	
	@GetMapping("/{season}")
	public ResponseEntity<List<Driver>> drivers2(@PathVariable int season) throws JsonMappingException, JsonProcessingException{
		
		List<Driver> drivers = driverService.findAllBySeason(season);
		
		return new ResponseEntity<>(drivers,HttpStatus.OK);
		
	}
	
	
	@GetMapping("/{id}")
    public ResponseEntity<Driver> getOne(@PathVariable String id){
        Driver driver = driverService.findOne(id);
        System.out.println(driver);
       
        if(driver != null) {
            return new ResponseEntity<>(driver, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
